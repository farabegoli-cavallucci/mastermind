package it.unibo.pcd.resolver

import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers._

class SimpleMastermindResolverTest extends AnyFreeSpec {
  private val resolver = new SimpleMastermindResolver(3, List("A"), List(1, 2, 3))
  private val resolverRepeated = new SimpleMastermindResolver(3, List(), List(1, 3, 1))

  "resolver" - {
    "should answer to guess (1, 3, 2) with (1, 2)" in {
      assert(resolver.answerToGuess(List(1, 3, 2)) == (1, 2))
    }

    "should answer to guess (5, 6, 7) with (0, 0)" in {
      assert(resolver.answerToGuess(List(5, 6, 7)) == (0, 0))
    }

    "should answer to guess (1, 2, 5) with (2, 0)" in {
      assert(resolver.answerToGuess(List(1, 2, 5)) == (2, 0))
    }

    "should answare to guess (1, 2, 3) with (3, 0)" in {
      assert(resolver.answerToGuess(List(1, 2, 3)) == (3, 0))
    }
  }

  "resolver with at least 2 digit repeated" - {
    "should answer to guess (1, 2, 3) with (1, 1)" in {
      assert(resolverRepeated.answerToGuess(List(1, 2, 3)) == (1, 1))
    }

    "should answer to guess (3, 1, 3) with (0, 3)" in {
      assert(resolverRepeated.answerToGuess(List(3, 1, 3)) == (0, 3))
    }

    "should answer to guess (1, 3, 1) with (3, 0)" in {
      assert(resolverRepeated.answerToGuess(List(1, 3, 1)) == (3, 0))
    }

    "should answer to guess (2, 2, 4) with (0, 0)" in {
      assert(resolverRepeated.answerToGuess(List(2, 2, 4)) == (0, 0))
    }
  }

  "resolver" - {
    "not ready to communicate the winning sequence if receive (1, 2)" in {
      val res = new SimpleMastermindResolver(3, List("A"), List(1, 2, 3))
      res.guess("A")
      res.computeAnswer("A", (1, 2))
      assert(!res.allSequences)
    }
    "ready to communicate the winning sequence if receive (3, 0)" in {
      val res = new SimpleMastermindResolver(3, List("A"), List(1, 2, 3))
      /* A sequence = (1, 2, 3) */
      res.guess("A")
      res.computeAnswer("A", (3, 0))
      assert(res.allSequences)
    }
  }
}
