package it.unibo.pcd.resolver

import org.scalatest.freespec.AnyFreeSpec

class SequenceGeneratorTest extends AnyFreeSpec {
  "The simpleSequence" - {
    "must not have digits > 10" in {
      assert(SequenceGenerator.simpleSequence(10).forall(_ / 10 == 0))
    }
  }

  "The uniqueSequence" - {
    "must have all digits distinct" in {
      val distList = SequenceGenerator.uniqueSequence(10)
      assert(distList.distinct.size == distList.size)
    }

    "digits > 10 must throw IllegalArgumentException" in {
      intercept[IllegalArgumentException] {
        SequenceGenerator.uniqueSequence(12)
      }
    }
  }
}
