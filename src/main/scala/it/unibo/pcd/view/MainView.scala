package it.unibo.pcd.view

import akka.actor.typed.{ActorSystem, Behavior}
import akka.actor.typed.scaladsl.Behaviors
import it.unibo.pcd.Messages.{Abort, FinishGame, LogToUi, NewGame, UiMessage}
import it.unibo.pcd.actor.JudgeActor
import javafx.beans.property.SimpleStringProperty
import javafx.fxml.FXML
import org.slf4j.{Logger, LoggerFactory}
import scalafx.application.Platform
import scalafx.event.ActionEvent
import scalafx.scene.control.Alert.AlertType
import scalafx.scene.control.{Alert, Button, TextArea, TextField}
import scalafxml.core.macros.sfxml

@sfxml
class MainView
(
  @FXML private val startButton: Button,
  @FXML private val stopButton: Button,
  private val digitsNumber: TextField,
  private val playersNumber: TextField,
  private val logArea: TextArea
) {
  private val logger: Logger = LoggerFactory.getLogger("MainView")
  private val uiActor = ActorSystem(UiActor(), "UiActor")
  private val judge = ActorSystem(JudgeActor(uiActor), "Judge")
  private val messages = new SimpleStringProperty(".:: Mastermind ::.")

  messages.addListener((obs, newVal, oldVal) => {
    logArea.selectPositionCaret(logArea.getLength)
    logArea.deselect()
  })
  logArea.textProperty().bind(messages)

  def handlePlayGame(event: ActionEvent): Unit = {
    Platform.runLater {
      stopButton.disable = false
    }
    checkValidityInput(playersNumber.text.value, digitsNumber.text.value) match {
      case Some(err) =>
        logger.error(err)
        new Alert(AlertType.Error, err).showAndWait()
      case None =>
        val numPlayers = playersNumber.text.value.toInt
        val digits = digitsNumber.text.value.toInt
        logger.info(s"Game started with: ${playersNumber.text.value} players and ${digitsNumber.text.value} digits")
        judge ! NewGame(numPlayers, digits)
    }
  }

  def handleStopGame(event: ActionEvent): Unit = {
    judge ! Abort()
    logger.warn("Game aborted by the user")
    Platform.runLater { stopButton.disable = true }
  }

  private def checkValidityInput(playersString: String, digitsString: String): Option[String] = {
    try {
      val players = playersString.toInt
      val digits = digitsString.toInt

      if (players < 2) {
        Option.apply("Two player are required")
      } else if (digits < 1) {
        Option.apply("Negative value are not allow")
      } else {
        Option.empty
      }
    } catch {
      case ex: NumberFormatException =>
        Option.apply(s"${ex.getMessage} unable to parse to Int")
    }
  }

  object UiActor {
    def apply(): Behaviors.Receive[UiMessage] =
      Behaviors.receiveMessage {
        case FinishGame(actorRef) =>
          Platform.runLater {
            messages.setValue(messages.getValue + s"\n***** ${actorRef.path.name} WIN *****\n\n")
            stopButton.disable = true
          }
          Behaviors.same
        case LogToUi(message) =>
          Platform.runLater {
            messages.setValue(messages.getValue + "\n" + message)
          }
          Behaviors.same
      }
  }

}
