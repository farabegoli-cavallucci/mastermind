package it.unibo.pcd.actor

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorRef, Behavior}
import it.unibo.pcd.Messages._

import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Random

object JudgeActor {
  def apply(uiActor: ActorRef[UiMessage]): Behavior[JudgeMessage] = Behaviors.setup(ctx => {
    new JudgeActor(uiActor, ctx).init
  })
}

private class JudgeActor(uiActor: ActorRef[UiMessage], context: ActorContext[JudgeMessage]) {
  private val judgeName: String = "Judge"
  private var players: List[ActorRef[PlayerMessage]] = _
  private var validationMap: Map[ActorRef[PlayerMessage], Boolean] = Map.empty[ActorRef[PlayerMessage], Boolean]
  private var currentPlayer: ActorRef[PlayerMessage] = _
  private var playersReady = 0

  private case object TimeoutTimer

  context.log.info("Judge created")

  object RichCollections {

    implicit class RandomElem(player: List[ActorRef[PlayerMessage]]) {
      def takeRandomElement: ActorRef[PlayerMessage] = {
        Random.shuffle(player).head
      }
    }

  }

  private def init: Behavior[JudgeMessage] = {
    Behaviors.receiveMessage {
      case NewGame(playersNumber, digit) =>
        context.log.info("New game... creating players")
        playersReady = 0
        val playersRef = for (i <- 0 until playersNumber)
          yield context.spawn(PlayerActor(context.self, digit), s"Player-$i")
        players = playersRef.toList
        if (validationMap.nonEmpty) validationMap = Map.empty
        players.foreach(p => { // Communicate to players other players' ref
          val otherPlayers = players.filterNot(_ == p)
          p ! PlayersRef(otherPlayers)
        })
        waitingPlayers
      case Abort() => abortAllPlayers
      case _ => Behaviors.unhandled
    }
  }

  private def waitingPlayers: Behavior[JudgeMessage] = {
    Behaviors.receiveMessage {
      case Ready() if playersReady < players.size - 1 =>
        context.log.info("Player ready")
        playersReady += 1
        Behaviors.same
      case Abort() => abortAllPlayers
      case _ => turning
    }
  }

  private def turning: Behavior[JudgeMessage] = {
    import RichCollections._
    currentPlayer = players.takeRandomElement
    context.log.info(s"Give turn to ${currentPlayer.path.name}")
    uiActor ! LogToUi(s"[$judgeName]: Give turn to ${currentPlayer.path.name}")
    currentPlayer ! GiveTurn()
    checkingTime
  }

  private def checkingTime: Behavior[JudgeMessage] = {
    Behaviors.withTimers[JudgeMessage] {
      timer =>
        timer.startSingleTimer(TimeoutTimer, Timeout(), 1 second)
        Behaviors.receiveMessage {
          case Timeout() =>
            context.log.info(s"[${currentPlayer.path.name}] has timeout")
            uiActor ! LogToUi(s"[${currentPlayer.path.name}] has timeout")
            removePlayer
          case GuessDone() =>
            context.log.info(s"${currentPlayer.path.name} is in time")
            timer.cancel(TimeoutTimer)
            waitingTargetPlayerResponse
          case WinAttempt(attempt) =>
            context.log.info(s"${currentPlayer.path.name} try to win!")
            uiActor ! LogToUi(s"[${currentPlayer.path.name}] try to win!")
            timer.cancel(TimeoutTimer)
            // For each actor send the win sequence to check
            // keyVal is the pair (actor, sequence)
            attempt.view.foreach(keyVal => keyVal._1 ! WinSequence(keyVal._2))
            validity
          case Abort() => abortAllPlayers
        }
    }
  }

  private def waitingTargetPlayerResponse: Behavior[JudgeMessage] = {
    Behaviors.receiveMessage {
      case ResultDone(_) =>
        context.log.info("Target player has finish to communicate the result. Going to next turn...")
        turning
      case Abort() => abortAllPlayers
    }
  }

  private def removePlayer: Behavior[JudgeMessage] = {
    currentPlayer ! Loose()
    players = players.filterNot(_ == currentPlayer)
    players.foreach(p => p ! PlayerLoose(currentPlayer))
    turning
  }

  private def ending: Behavior[JudgeMessage] = {
    context.log.info(s"$judgeName: ${currentPlayer.path.name} LOOSE!")
    uiActor ! LogToUi(s"[$judgeName]: ${currentPlayer.path.name} LOOSE!")
    currentPlayer ! Loose()
    players = players.filterNot(_ == currentPlayer)
    players.foreach(p => p ! GameEnd(p))
    uiActor ! FinishGame(players.head)
    init
  }

  private def currentPlayerWin: Behavior[JudgeMessage] = {
    context.log.info(s"$judgeName: ${currentPlayer.path.name} WIN")
    uiActor ! LogToUi(s"[$judgeName]: ${currentPlayer.path.name} WIN")
    currentPlayer ! Win()
    uiActor ! FinishGame(currentPlayer)
    players.filterNot(_ == currentPlayer).foreach(_ ! GameEnd(currentPlayer))
    init
  }

  private def validity: Behavior[JudgeMessage] = {
    context.log.info(s"$judgeName: verify if all the sequences are valid...")
    uiActor ! LogToUi(s"[$judgeName]: verify if all the sequences are valid...")
    Behaviors.receiveMessage {
      case isSequenceValid(valid, actorRef) =>
        validationMap = validationMap ++ Map(actorRef -> valid)
        if (validationMap.size == players.size - 1) { // All players sent the isSequenceValid() message
          if (players.size == 2) { // Remaining only 2 players
            context.log.info(s"$judgeName: only 2 player are remaining")
            verifyTwoPlayers(validationMap.values)
          } else {
            verifyAllPlayers(validationMap.values)
          }
        } else {
          Behaviors.same
        }
      case Abort() => abortAllPlayers
    }
  }

  private def verifyTwoPlayers(validationValues: Iterable[Boolean]): Behavior[JudgeMessage] = {
    if (validationValues.forall(e => e)) { // The currentPlayer win: all sequence are true
      currentPlayerWin
    } else {
      ending
    }
  }

  private def verifyAllPlayers(validationValues: Iterable[Boolean]): Behavior[JudgeMessage] = {
    if (validationValues.forall(e => e)) {
      currentPlayerWin
    } else {
      removePlayer
    }
  }

  private def abortAllPlayers: Behavior[JudgeMessage] = {
    players.foreach(_ ! Abort())
    init
  }
}
