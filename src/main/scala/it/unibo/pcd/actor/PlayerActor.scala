package it.unibo.pcd.actor

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import it.unibo.pcd.Messages._
import it.unibo.pcd.resolver.{MastermindResolver, SequenceGenerator, SimpleMastermindResolver}

import scala.util.Random

object PlayerActor {
  def apply(judge: ActorRef[JudgeMessage], digits: Int): Behavior[PlayerMessage] = Behaviors.setup(ctx => {
    new PlayerActor(ctx, judge, digits).init
  })
}

private class PlayerActor(context: ActorContext[PlayerMessage], judge: ActorRef[JudgeMessage], digits: Int) {
  private val playerName: String = context.self.path.name
  private var players: List[ActorRef[PlayerMessage]] = _
  private var resolver: MastermindResolver[ActorRef[PlayerMessage]] = _

  context.log.info(s"Player ${context.self.path.name} spawned")

  private def init: Behavior[PlayerMessage] = {
    Behaviors.receiveMessage {
      case PlayersRef(playersRef) =>
        players = playersRef
        resolver =
          new SimpleMastermindResolver(digits, players, SequenceGenerator.uniqueSequence(digits))
        judge ! Ready()
        waiting
      case Abort() => Behaviors.stopped(() => context.log.info(s"$playerName: Abort"))
    }
  }

  private def waiting: Behavior[PlayerMessage] = {
    Behaviors.receiveMessage {
      case GiveTurn() => playing
      case Guess(seq) =>
        val result = resolver.answerToGuess(seq)
        context.log.info(s"$playerName: ehmmm someone try to guess me... result is (${result._1}, ${result._2})")
        players.foreach(p => p ! GuessResult(context.self, seq, result._1, result._2))
        judge ! ResultDone(context.self)
        Behaviors.same
      case WinSequence(seq) =>
        judge ! isSequenceValid(resolver.isSequenceValid(seq), context.self)
        Behaviors.same
      case GuessResult(actorRef, seq, inPlace, notInPlace) =>
        context.log.info(s"$playerName: someone guess... take info")
        resolver.computeExternalAnswer(actorRef, seq, (inPlace, notInPlace))
        Behaviors.same
      case GameEnd(actorRef) => if (actorRef == context.self) gracefullyStopActor(win) else gracefullyStopActor(loose)
      case PlayerLoose(actorRef) =>
        players = players.filterNot(_ == actorRef)
        Behaviors.same
      case Abort() => Behaviors.stopped(() => context.log.info(s"$playerName: Abort"))
    }
  }

  private def playing: Behavior[PlayerMessage] = {
    context.log.info(s"$playerName: It's my turn")
    if (resolver.allSequences) guessAllPlayers else guessSinglePlayer
  }

  private def guessAllPlayers: Behavior[PlayerMessage] = {
    judge ! WinAttempt(resolver.getAllSequences)
    Behaviors.receiveMessage {
      case Loose() => gracefullyStopActor(loose)
      case Win() => gracefullyStopActor(win)
      case Timeout() => gracefullyStopActor(timeout)
      case Abort() => Behaviors.stopped(() => context.log.info(s"$playerName: Abort"))
    }
  }

  private def guessSinglePlayer: Behavior[PlayerMessage] = {
    val playerGuessed = Random.shuffle(players).head
    context.log.info(s"$playerName: single guess to ${playerGuessed.path.name}")
    judge ! GuessDone()
    playerGuessed ! Guess(resolver.guess(playerGuessed))
    Behaviors.receiveMessage {
      case GuessResult(actorRef, _, inPlace, notInPlace) =>
        context.log.info(s"$playerName: get result from ${actorRef.path.name} with ($inPlace, $notInPlace)")
        resolver.computeAnswer(actorRef, (inPlace, notInPlace))
        waiting
      case Timeout() => gracefullyStopActor(timeout)
      case Abort() => Behaviors.stopped(() => context.log.info(s"$playerName: Abort"))
    }
  }

  private def gracefullyStopActor(postStop: () => Unit): Behavior[PlayerMessage] = Behaviors.stopped(postStop)

  private val timeout: () => Unit = () => context.log.info(s"$playerName: Time is over, I loose")
  private val win: () => Unit = () => context.log.info(s"$playerName: I Win!")
  private val loose: () => Unit = () => context.log.info(s"$playerName: I Loose!")
}
