package it.unibo.pcd

import java.io.IOException
import java.net.URL

import javafx.scene.Parent
import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.scene.Scene
import scalafxml.core.{FXMLView, NoDependencyResolver}

object Mastermind extends JFXApp {
  val resource: URL = getClass.getResource("/fxml/MainView.fxml")
  if (resource == null) {
    throw new IOException("Cannot load resource: MainView.fxml")
  }

  val root: Parent = FXMLView(resource, NoDependencyResolver)

  stage = new PrimaryStage() {
    title = "Collective Mastermind"
    scene = new Scene(root)
  }
}
