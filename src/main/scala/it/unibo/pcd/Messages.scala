package it.unibo.pcd

import akka.actor.typed.ActorRef

object Messages {

  /**
   * Base message managed by the Judge.
   */
  sealed trait JudgeMessage

  /**
   * Base message managed by the player.
   */
  sealed trait PlayerMessage

  /**
   * Base message managed by the UI agent and Judge.
   */
  sealed trait UiMessage

  /**
   * Message to Player from another Player for guessing the sequence, this message is also propagate to the Judge.
   *
   * @param sequence to guess.
   */
  final case class Guess(sequence: List[Int]) extends PlayerMessage

  /**
   * Message from Player to another Player for communicate the guessing result.
   *
   * @param actorRef   the reference to the "guessed" actor.
   * @param seq        : the guess sequence
   * @param inPlace    : how many digit are in the correct position.
   * @param notInPlace : how many digit are present but in the incorrect position.
   */
  final case class GuessResult(actorRef: ActorRef[PlayerMessage], seq: List[Int], inPlace: Int, notInPlace: Int) extends PlayerMessage

  /**
   * Message to the Judge with the winning's attempt.
   *
   * @param attempt : list of guessing sequence. (the position of the outer list represent the player).
   */
  final case class WinAttempt(attempt: Map[ActorRef[PlayerMessage], List[Int]]) extends JudgeMessage with PlayerMessage

  /**
   * Message from source Player to Judge.
   * This message is used to inform the Judge that the source Player has done the guess.
   * This message is used by the Judge to verify that se source player guessed the target player before the timeout.
   */
  final case class GuessDone() extends JudgeMessage with PlayerMessage

  /**
   * This message is sent by the target Player when it has finished to communicate the guess' result.
   *
   * @param actorRef target Player ref.
   */
  final case class ResultDone(actorRef: ActorRef[PlayerMessage]) extends JudgeMessage with PlayerMessage

  /**
   * Message sent to Judge to other player to communicate the numbers of Players in the current session.
   *
   * @param totalPlayer number of player in the current session.
   */
  final case class PlayerNumbers(totalPlayer: Int) extends JudgeMessage with PlayerMessage

  /**
   * Message from Judge to player that play in the turn.
   */
  final case class GiveTurn() extends JudgeMessage with PlayerMessage

  /**
   * Message from Judge to Player if it has win.
   */
  final case class Win() extends JudgeMessage with PlayerMessage

  /**
   * Message from Judge to Player if it has loose.
   */
  final case class Loose() extends JudgeMessage with PlayerMessage

  /**
   * Message from Judge to all Player to communicate the guess sequence.
   *
   * @param sequence that it will be guess from all player.
   */
  final case class WinSequence(sequence: List[Int]) extends JudgeMessage with PlayerMessage

  /**
   * Message from Judge to Player if the timeout is expired.
   */
  final case class Timeout() extends JudgeMessage with PlayerMessage

  /**
   * Message from Player to Judge for communicate if the win sequence is valid.
   *
   * @param isValid : if the sequence is valid.
   */
  final case class isSequenceValid(isValid: Boolean, actorRef: ActorRef[PlayerMessage]) extends JudgeMessage with PlayerMessage

  /**
   * Message sent by the judge to all player to notify the game is over.
   *
   * @param actorRef the winning player.
   */
  final case class GameEnd(actorRef: ActorRef[PlayerMessage]) extends JudgeMessage with PlayerMessage

  /**
   * Message sent by the judge to other player to notify that one player is not in the game.
   *
   * @param actorRef the loose player.
   */
  final case class PlayerLoose(actorRef: ActorRef[PlayerMessage]) extends JudgeMessage with PlayerMessage

  /**
   * Message sent by UI agent to the judge to notify a new game start.
   *
   * @param playersNumber number of players in the game.
   * @param digits        number of digits to guess.
   */
  final case class NewGame(playersNumber: Int, digits: Int) extends UiMessage with JudgeMessage

  /**
   * Message sent from judge to uiActor to notify the game's end.
   *
   * @param actorRef the winner player.
   */
  final case class FinishGame(actorRef: ActorRef[PlayerMessage]) extends UiMessage with JudgeMessage

  /**
   * Message sent from judge to uiActor to print a message to Ui.
   *
   * @param message the message to print.
   */
  final case class LogToUi(message: String) extends UiMessage with JudgeMessage

  /**
   * Message sent by the judge to all the players to communicate other players ref.
   *
   * @param players other players ref.
   */
  final case class PlayersRef(players: List[ActorRef[PlayerMessage]]) extends JudgeMessage with PlayerMessage

  /**
   * Message sent by the player to the judge to notify that is ready.
   */
  final case class Ready() extends JudgeMessage with PlayerMessage

  /**
   * Message used to abort the game in any circumstance.
   * Is typically sent by UiActor when the user quit the game.
   */
  final case class Abort() extends UiMessage with JudgeMessage with PlayerMessage
}
