package it.unibo.pcd.resolver

trait MastermindResolver[A] {
  val digits: Int
  var players: List[A]

  /* Outcome methods */
  def guess(player: A): List[Int]

  def answerToGuess(guess: List[Int]): (Int, Int) // The pair mean (inPlace, notInPlace)
  def getAllSequences: Map[A, List[Int]]

  /* Income methods */
  def computeExternalAnswer(player: A, seq: List[Int], answer: (Int, Int)): Unit

  def computeAnswer(player: A, answer: (Int, Int)): Unit

  def isSequenceValid(guess: List[Int]): Boolean

  /* Internal methods */
  def allSequences: Boolean
}
