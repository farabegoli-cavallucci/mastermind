package it.unibo.pcd.resolver

import scala.util.Random

object SequenceGenerator {
  private val digitsList = List(0, 1, 2, 3, 4, 5, 6, 7, 8, 9) // scalastyle:ignore
  def simpleSequence(digits: Int): List[Int] = Seq.fill(digits)(Math.abs(Random.nextInt % 10)).toList

  def uniqueSequence(digits: Int): List[Int] = digits match {
    case seq if seq <= 10 => Random.shuffle(digitsList).take(seq)
    case _ => throw new IllegalArgumentException("Unable to generate a sequence with more than 10 digits")
  }
}
