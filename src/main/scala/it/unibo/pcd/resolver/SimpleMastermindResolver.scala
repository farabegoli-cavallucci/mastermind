package it.unibo.pcd.resolver

import scala.language.postfixOps
import scala.util.Random

class SimpleMastermindResolver[A]
(
  override val digits: Int,
  override var players: List[A],
  var sequence: List[Int]
) extends MastermindResolver[A] {

  type Possibilities = Set[List[Int]]

  object Constant {
    val RANGE = 10
  }

  private var currentGuess: List[Int] = _
  private var allPossibilities: Map[A, Possibilities] =
    players map (p => p ->
      (0 until Math.pow(Constant.RANGE, digits).toInt)
        .map(e => s"%0${digits}d".format(e))
        .map(e => e.map(x => x.asDigit).toList)
        .filter(e => e.distinct.size == e.size) // Take only the lists with distinct elements
        .toSet
      ) toMap

  /* Outcome methods */
  override def guess(player: A): List[Int] = {
    currentGuess = Random.shuffle(allPossibilities(player)).head
    currentGuess
  }

  override def answerToGuess(guess: List[Int]): (Int, Int) = {
    answerToTarget(sequence, guess)
  }

  override def getAllSequences: Map[A, List[Int]] = {
    if (allPossibilities.values.forall(_.size == 1)) {
      allPossibilities.map(e => e._1 -> e._2.head)
    } else {
      throw new IllegalStateException()
    }
  }

  private def answerToTarget(target: List[Int], guess: List[Int]): (Int, Int) = {
    var inPlace: Int = 0
    var notInPlace: Int = 0
    target.zipWithIndex.foreach(v => {
      if (v._1 == guess(v._2)) {
        inPlace += 1
      } else {
        if (target.contains(guess(v._2))) notInPlace += 1
      }
    })
    (inPlace, notInPlace)
  }

  /* Income methods */
  override def computeExternalAnswer(player: A, seq: List[Int], answer: (Int, Int)): Unit = {
    val newPossibilities = allPossibilities(player).filter(answerToTarget(_, seq) == answer)
    allPossibilities = allPossibilities + (player -> newPossibilities)
  }

  override def computeAnswer(player: A, answer: (Int, Int)): Unit = {
    computeExternalAnswer(player, currentGuess, answer)
  }

  /* internal method */
  override def allSequences: Boolean = allPossibilities.values.forall(_.size == 1)

  override def isSequenceValid(guess: List[Int]): Boolean = sequence == guess
}
